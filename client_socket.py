import socket
import re, uuid
HEADER = 64
PORT = 3074
#SERVER = '192.168.1.5'
SERVER = socket.gethostbyname('cesarskatelife.ddns.net')
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'


MAC = (':'.join(re.findall('..', '%012x' % uuid.getnode()))) 

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

send(MAC)
while True:
    mensaje = input("mensaje a enviar: ")
    send(mensaje)
    send(DISCONNECT_MESSAGE)
    break

