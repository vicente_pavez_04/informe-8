import socket
import threading
from cryptography.fernet import Fernet

HEADER = 64
PORT = 3074
SERVER = '192.168.1.5'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

def load_key():
    """
    Loads the key named `secret.key` from the current directory.
    """
    return open("secret.key", "rb").read()


key = load_key()



server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} Autorizando.")
    connected = True
    i = 0
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT) #Tamaño del mensaje encriptado
        if msg_length:
            msg_length = int(msg_length) 
            msg = conn.recv(msg_length) #recibe el mensaje encriptado con su tamaño correspondiente
            f = Fernet(key)
            decrypted_message = f.decrypt(msg) #desencripta el mensaje
            msg = decrypted_message.decode(FORMAT) #decodifica el mensaje





            if i == 0:
                if msg != "b8:27:eb:81:a8:7f":
                    conn.send("Conexion fallida".encode(FORMAT))
                    connected = False

                print(f"[NEW CONNECTION] {addr} Conectado.")  
                conn.send("Conexion establecida".encode(FORMAT))

            if msg == DISCONNECT_MESSAGE :
                connected = False
                conn.send("Conexion finalizada".encode(FORMAT))

            if i != 0:
                print(f"[{addr}] {msg}")
                conn.send("Msg received".encode(FORMAT))
            
            
            i += 1

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
